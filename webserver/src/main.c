#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/wait.h>
#include <linux/limits.h>
#include <string.h>
#include "../include/getreq-sendreq.h"

#define buffSize 4096
#define defaultServerPort 4747
#define SA struct sockaddr
#define CONNECTIONS 1000
#define serverRootDirectory "../www/"

int connectionSocketDescriptor[CONNECTIONS], n;
char *request;

int main (int argc, char *argv[]) {
        int Connection = 0, port = defaultServerPort, option = 0;
        struct sockaddr_in connectionAddress;
        socklen_t connectionAddressLength = sizeof(connectionAddress);

        while((option = getopt(argc, argv, "p:h")) != -1)
        {
                switch(option)
                {
                case 'p':
                        port = GetPort(argv[2], defaultServerPort);
                        break;
                case 'h':
                        helpF();
                        break;
                default:
                        helpF();
                        break;
                }
        }
        EmptyconnectionSocketDescriptorF();
        int serverSocketDescriptor = createServer(port);

        while (1)
        {
                printf("Waiting for a connection on a port %d \n\n", port);
                connectionSocketDescriptor[Connection] = accept(serverSocketDescriptor, (SA *) &connectionAddress, &connectionAddressLength);
                if(connectionSocketDescriptor[Connection] < 0)
                {
                        perror("Connection in not established");
                }
                else
                {
                        request = malloc(sizeof(char) * buffSize);
                        request = getRequest(connectionSocketDescriptor[Connection]);
                        pid_t pid = fork();//creating new process using fork

                        if ( pid == 0 )
                        {
                                close(serverSocketDescriptor);
                                sendResponse(connectionSocketDescriptor[Connection], request, serverRootDirectory);
                                close(connectionSocketDescriptor[Connection]);
                                exit(0);
                        }
                        else
                        {
                                waitpid(pid, NULL, 0);
                                close(connectionSocketDescriptor[Connection]);
                        }
                }
                connectionSocketDescriptor[Connection]=-1;      
                while (connectionSocketDescriptor[Connection]!=-1)
                        Connection = (Connection+1)%CONNECTIONS;
        }
        return 0;
}
