#ifndef getreq_sendreq_h
#define getreq_sendreq_h

char *createAbsolutePath(char *filename);
void printFileDetails(int socketDescriptor, int fileDescriptor, int statusCode, char *httpMethod);
int GetPort(char *port, int defaultport);
int createServer(int port);
char* getRequest(int getSocketDescriptor);
void sendResponse(int getSocketDescriptor, char *request, char *serverRootDirectoryParameter);
void EmptyconnectionSocketDescriptorF();
void helpF();

#endif
